/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     29/04/2018 16:17:19                          */
/*==============================================================*/


drop index RELATIONSHIP_2_FK;

drop index AMBIENTE_PK;

drop table AMBIENTE;

drop index RELATIONSHIP_27_FK;

drop index CALENDARIO_FECHAS_FESTIVAS_PK;

drop table CALENDARIO_FECHAS_FESTIVAS;

drop index RELATIONSHIP_22_FK;

drop index RELATIONSHIP_21_FK;

drop index RELATIONSHIP_20_FK;

drop index RELATIONSHIP_19_FK;

drop index RELATIONSHIP_18_FK;

drop index CAMBIO_PK;

drop table CAMBIO;

drop index CARGO_PK;

drop table CARGO;

drop index RELATIONSHIP_6_FK;

drop index CARRERA_PK;

drop table CARRERA;

drop index RELATIONSHIP_7_FK;

drop index DEPARTAMENTO_PK;

drop table DEPARTAMENTO;

drop index DIA_PK;

drop table DIA;

drop index ESTADO_SOLICITUD_PK;

drop table ESTADO_SOLICITUD;

drop index GESTION_PK;

drop table GESTION;

drop index RELATIONSHIP_11_FK;

drop index RELATIONSHIP_10_FK;

drop index RELATIONSHIP_9_FK;

drop index GRUPO_PK;

drop table GRUPO;

drop index RELATIONSHIP_17_FK;

drop index HISTORIAL_PK;

drop table HISTORIAL;

drop index RELATIONSHIP_15_FK;

drop index RELATIONSHIP_14_FK;

drop index RELATIONSHIP_13_FK;

drop index RELATIONSHIP_12_FK;

drop index HORARIO_PK;

drop table HORARIO;

drop index RELATIONSHIP_8_FK;

drop index MATERIA_PK;

drop table MATERIA;

drop index RELATIONSHIP_16_FK;

drop index NOTIFICACION_PK;

drop table NOTIFICACION;

drop index PERIODO_PK;

drop table PERIODO;

drop index RECURSO_PK;

drop table RECURSO;

drop index RELATIONSHIP_5_FK;

drop index RELATIONSHIP_4_FK;

drop index RECURSO_AMBIENTE_PK;

drop table RECURSO_AMBIENTE;

drop index RELATIONSHIP_26_FK;

drop index RELATIONSHIP_25_FK;

drop index RELATIONSHIP_24_FK;

drop index RELATIONSHIP_23_FK;

drop index SOLICITUD_CAMBIO_PK;

drop table SOLICITUD_CAMBIO;

drop index TIPOAMBIENTE_PK;

drop table TIPOAMBIENTE;

drop index TIPO_SOLICITUD_PK;

drop table TIPO_SOLICITUD;

drop index RELATIONSHIP_1_FK;

drop index USUARIO_PK;

drop table USUARIO;

/*==============================================================*/
/* Table: AMBIENTE                                              */
/*==============================================================*/
create table AMBIENTE (
   NOMBRE_AMBIENTE      TEXT                 not null,
   ID_TIPO_AMBIENTE     NUMERIC              null,
   CAPACIDAD            NUMERIC              null,
   UBICACION            TEXT                 null,
   constraint PK_AMBIENTE primary key (NOMBRE_AMBIENTE)
);

/*==============================================================*/
/* Index: AMBIENTE_PK                                           */
/*==============================================================*/
create unique index AMBIENTE_PK on AMBIENTE (
NOMBRE_AMBIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on AMBIENTE (
ID_TIPO_AMBIENTE
);

/*==============================================================*/
/* Table: CALENDARIO_FECHAS_FESTIVAS                            */
/*==============================================================*/
create table CALENDARIO_FECHAS_FESTIVAS (
   FECHA                DATE                 not null,
   ID_GESTION           NUMERIC              null,
   DESCRIPCION          TEXT                 null,
   constraint PK_CALENDARIO_FECHAS_FESTIVAS primary key (FECHA)
);

/*==============================================================*/
/* Index: CALENDARIO_FECHAS_FESTIVAS_PK                         */
/*==============================================================*/
create unique index CALENDARIO_FECHAS_FESTIVAS_PK on CALENDARIO_FECHAS_FESTIVAS (
FECHA
);

/*==============================================================*/
/* Index: RELATIONSHIP_27_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_27_FK on CALENDARIO_FECHAS_FESTIVAS (
ID_GESTION
);

/*==============================================================*/
/* Table: CAMBIO                                                */
/*==============================================================*/
create table CAMBIO (
   ID_CAMBIO            NUMERIC              not null,
   ID_HORARIO           NUMERIC              null,
   ID_PERIODO           NUMERIC              null,
   NOMBRE_AMBIENTE      TEXT                 null,
   ID_DIA               NUMERIC              null,
   CODSIS               NUMERIC              null,
   constraint PK_CAMBIO primary key (ID_CAMBIO)
);

/*==============================================================*/
/* Index: CAMBIO_PK                                             */
/*==============================================================*/
create unique index CAMBIO_PK on CAMBIO (
ID_CAMBIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_18_FK on CAMBIO (
ID_HORARIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_19_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_19_FK on CAMBIO (
ID_PERIODO
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_20_FK on CAMBIO (
NOMBRE_AMBIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_21_FK on CAMBIO (
ID_DIA
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_22_FK on CAMBIO (
CODSIS
);

/*==============================================================*/
/* Table: CARGO                                                 */
/*==============================================================*/
create table CARGO (
   ID_CARGO             NUMERIC              not null,
   NOMBRE               TEXT                 null,
   constraint PK_CARGO primary key (ID_CARGO)
);

/*==============================================================*/
/* Index: CARGO_PK                                              */
/*==============================================================*/
create unique index CARGO_PK on CARGO (
ID_CARGO
);

/*==============================================================*/
/* Table: CARRERA                                               */
/*==============================================================*/
create table CARRERA (
   COD_CARRERA          NUMERIC              not null,
   ID_DEPARTAMENTO      NUMERIC              null,
   NOMBRE_CARRERA       TEXT                 null,
   constraint PK_CARRERA primary key (COD_CARRERA)
);

/*==============================================================*/
/* Index: CARRERA_PK                                            */
/*==============================================================*/
create unique index CARRERA_PK on CARRERA (
COD_CARRERA
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on CARRERA (
ID_DEPARTAMENTO
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO (
   ID_DEPARTAMENTO      NUMERIC              not null,
   CODSIS               NUMERIC              null,
   NOMBRE_DEPARTAMENTO  TEXT                 null,
   constraint PK_DEPARTAMENTO primary key (ID_DEPARTAMENTO)
);

/*==============================================================*/
/* Index: DEPARTAMENTO_PK                                       */
/*==============================================================*/
create unique index DEPARTAMENTO_PK on DEPARTAMENTO (
ID_DEPARTAMENTO
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on DEPARTAMENTO (
CODSIS
);

/*==============================================================*/
/* Table: DIA                                                   */
/*==============================================================*/
create table DIA (
   ID_DIA               NUMERIC              not null,
   NOMBRE               TEXT                 null,
   constraint PK_DIA primary key (ID_DIA)
);

/*==============================================================*/
/* Index: DIA_PK                                                */
/*==============================================================*/
create unique index DIA_PK on DIA (
ID_DIA
);

/*==============================================================*/
/* Table: ESTADO_SOLICITUD                                      */
/*==============================================================*/
create table ESTADO_SOLICITUD (
   ID_SOLICITUD         NUMERIC              not null,
   NOMBRE               TEXT                 null,
   constraint PK_ESTADO_SOLICITUD primary key (ID_SOLICITUD)
);

/*==============================================================*/
/* Index: ESTADO_SOLICITUD_PK                                   */
/*==============================================================*/
create unique index ESTADO_SOLICITUD_PK on ESTADO_SOLICITUD (
ID_SOLICITUD
);

/*==============================================================*/
/* Table: GESTION                                               */
/*==============================================================*/
create table GESTION (
   ID_GESTION           NUMERIC              not null,
   NOMBRE               TEXT                 null,
   constraint PK_GESTION primary key (ID_GESTION)
);

/*==============================================================*/
/* Index: GESTION_PK                                            */
/*==============================================================*/
create unique index GESTION_PK on GESTION (
ID_GESTION
);

/*==============================================================*/
/* Table: GRUPO                                                 */
/*==============================================================*/
create table GRUPO (
   ID_GRUPO             NUMERIC              not null,
   COD_MATERIA          NUMERIC              null,
   ID_GESTION           NUMERIC              null,
   CODSIS               NUMERIC              null,
   NUMERO_GRUPO         NUMERIC              null,
   NUMERO_INSCRITOS     NUMERIC              null,
   constraint PK_GRUPO primary key (ID_GRUPO)
);

/*==============================================================*/
/* Index: GRUPO_PK                                              */
/*==============================================================*/
create unique index GRUPO_PK on GRUPO (
ID_GRUPO
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on GRUPO (
COD_MATERIA
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on GRUPO (
ID_GESTION
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_11_FK on GRUPO (
CODSIS
);

/*==============================================================*/
/* Table: HISTORIAL                                             */
/*==============================================================*/
create table HISTORIAL (
   ID_HISTORIAL         NUMERIC              not null,
   CODSIS               NUMERIC              null,
   ACCION               TEXT                 null,
   FECHA_HORA           DATE                 null,
   constraint PK_HISTORIAL primary key (ID_HISTORIAL)
);

/*==============================================================*/
/* Index: HISTORIAL_PK                                          */
/*==============================================================*/
create unique index HISTORIAL_PK on HISTORIAL (
ID_HISTORIAL
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_17_FK on HISTORIAL (
CODSIS
);

/*==============================================================*/
/* Table: HORARIO                                               */
/*==============================================================*/
create table HORARIO (
   ID_HORARIO           NUMERIC              not null,
   ID_PERIODO           NUMERIC              null,
   NOMBRE_AMBIENTE      TEXT                 null,
   ID_GRUPO             NUMERIC              null,
   ID_DIA               NUMERIC              null,
   constraint PK_HORARIO primary key (ID_HORARIO)
);

/*==============================================================*/
/* Index: HORARIO_PK                                            */
/*==============================================================*/
create unique index HORARIO_PK on HORARIO (
ID_HORARIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_12_FK on HORARIO (
ID_PERIODO
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_13_FK on HORARIO (
NOMBRE_AMBIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_14_FK on HORARIO (
ID_GRUPO
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_15_FK on HORARIO (
ID_DIA
);

/*==============================================================*/
/* Table: MATERIA                                               */
/*==============================================================*/
create table MATERIA (
   COD_MATERIA          NUMERIC              not null,
   COD_CARRERA          NUMERIC              null,
   NOMBRE               TEXT                 null,
   constraint PK_MATERIA primary key (COD_MATERIA)
);

/*==============================================================*/
/* Index: MATERIA_PK                                            */
/*==============================================================*/
create unique index MATERIA_PK on MATERIA (
COD_MATERIA
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on MATERIA (
COD_CARRERA
);

/*==============================================================*/
/* Table: NOTIFICACION                                          */
/*==============================================================*/
create table NOTIFICACION (
   ID_NOTIFICACION      NUMERIC              not null,
   CODSIS               NUMERIC              null,
   TITULO               TEXT                 null,
   CONTENIDO            TEXT                 null,
   FECHA_HORA           DATE                 null,
   constraint PK_NOTIFICACION primary key (ID_NOTIFICACION)
);

/*==============================================================*/
/* Index: NOTIFICACION_PK                                       */
/*==============================================================*/
create unique index NOTIFICACION_PK on NOTIFICACION (
ID_NOTIFICACION
);

/*==============================================================*/
/* Index: RELATIONSHIP_16_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_16_FK on NOTIFICACION (
CODSIS
);

/*==============================================================*/
/* Table: PERIODO                                               */
/*==============================================================*/
create table PERIODO (
   ID_PERIODO           NUMERIC              not null,
   HR_INICIO            TIME                 null,
   HR_FIN               TIME                 null,
   constraint PK_PERIODO primary key (ID_PERIODO)
);

/*==============================================================*/
/* Index: PERIODO_PK                                            */
/*==============================================================*/
create unique index PERIODO_PK on PERIODO (
ID_PERIODO
);

/*==============================================================*/
/* Table: RECURSO                                               */
/*==============================================================*/
create table RECURSO (
   ID_RECURSO           NUMERIC              not null,
   NOMBRE_RECURSO       TEXT                 null,
   constraint PK_RECURSO primary key (ID_RECURSO)
);

/*==============================================================*/
/* Index: RECURSO_PK                                            */
/*==============================================================*/
create unique index RECURSO_PK on RECURSO (
ID_RECURSO
);

/*==============================================================*/
/* Table: RECURSO_AMBIENTE                                      */
/*==============================================================*/
create table RECURSO_AMBIENTE (
   ID_RECURSO           NUMERIC              not null,
   NOMBRE_AMBIENTE      TEXT                 not null,
   constraint PK_RECURSO_AMBIENTE primary key (ID_RECURSO, NOMBRE_AMBIENTE)
);

/*==============================================================*/
/* Index: RECURSO_AMBIENTE_PK                                   */
/*==============================================================*/
create unique index RECURSO_AMBIENTE_PK on RECURSO_AMBIENTE (
ID_RECURSO,
NOMBRE_AMBIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on RECURSO_AMBIENTE (
NOMBRE_AMBIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on RECURSO_AMBIENTE (
ID_RECURSO
);

/*==============================================================*/
/* Table: SOLICITUD_CAMBIO                                      */
/*==============================================================*/
create table SOLICITUD_CAMBIO (
   ID_SOLICITUD         NUMERIC              not null,
   ID_CAMBIO            NUMERIC              null,
   ID_TIPO_SOLICITUD    NUMERIC              null,
   EST_ID_SOLICITUD     NUMERIC              null,
   ID_GESTION           NUMERIC              null,
   FECHA_HORA           DATE                 null,
   constraint PK_SOLICITUD_CAMBIO primary key (ID_SOLICITUD)
);

/*==============================================================*/
/* Index: SOLICITUD_CAMBIO_PK                                   */
/*==============================================================*/
create unique index SOLICITUD_CAMBIO_PK on SOLICITUD_CAMBIO (
ID_SOLICITUD
);

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_23_FK on SOLICITUD_CAMBIO (
ID_CAMBIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_24_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_24_FK on SOLICITUD_CAMBIO (
ID_TIPO_SOLICITUD
);

/*==============================================================*/
/* Index: RELATIONSHIP_25_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_25_FK on SOLICITUD_CAMBIO (
EST_ID_SOLICITUD
);

/*==============================================================*/
/* Index: RELATIONSHIP_26_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_26_FK on SOLICITUD_CAMBIO (
ID_GESTION
);

/*==============================================================*/
/* Table: TIPOAMBIENTE                                          */
/*==============================================================*/
create table TIPOAMBIENTE (
   ID_TIPO_AMBIENTE     NUMERIC              not null,
   NOMBRE_TIPO          TEXT                 null,
   constraint PK_TIPOAMBIENTE primary key (ID_TIPO_AMBIENTE)
);

/*==============================================================*/
/* Index: TIPOAMBIENTE_PK                                       */
/*==============================================================*/
create unique index TIPOAMBIENTE_PK on TIPOAMBIENTE (
ID_TIPO_AMBIENTE
);

/*==============================================================*/
/* Table: TIPO_SOLICITUD                                        */
/*==============================================================*/
create table TIPO_SOLICITUD (
   ID_TIPO_SOLICITUD    NUMERIC              not null,
   NOMBRE               TEXT                 null,
   constraint PK_TIPO_SOLICITUD primary key (ID_TIPO_SOLICITUD)
);

/*==============================================================*/
/* Index: TIPO_SOLICITUD_PK                                     */
/*==============================================================*/
create unique index TIPO_SOLICITUD_PK on TIPO_SOLICITUD (
ID_TIPO_SOLICITUD
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   CODSIS               NUMERIC              not null,
   ID_CARGO             NUMERIC              null,
   NOMBRE               TEXT                 null,
   EMAIL                TEXT                 null,
   TELEFONO             NUMERIC              null,
   constraint PK_USUARIO primary key (CODSIS)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
CODSIS
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on USUARIO (
ID_CARGO
);

alter table AMBIENTE
   add constraint FK_AMBIENTE_RELATIONS_TIPOAMBI foreign key (ID_TIPO_AMBIENTE)
      references TIPOAMBIENTE (ID_TIPO_AMBIENTE)
      on delete restrict on update restrict;

alter table CALENDARIO_FECHAS_FESTIVAS
   add constraint FK_CALENDAR_RELATIONS_GESTION foreign key (ID_GESTION)
      references GESTION (ID_GESTION)
      on delete restrict on update restrict;

alter table CAMBIO
   add constraint FK_CAMBIO_RELATIONS_HORARIO foreign key (ID_HORARIO)
      references HORARIO (ID_HORARIO)
      on delete restrict on update restrict;

alter table CAMBIO
   add constraint FK_CAMBIO_RELATIONS_PERIODO foreign key (ID_PERIODO)
      references PERIODO (ID_PERIODO)
      on delete restrict on update restrict;

alter table CAMBIO
   add constraint FK_CAMBIO_RELATIONS_AMBIENTE foreign key (NOMBRE_AMBIENTE)
      references AMBIENTE (NOMBRE_AMBIENTE)
      on delete restrict on update restrict;

alter table CAMBIO
   add constraint FK_CAMBIO_RELATIONS_DIA foreign key (ID_DIA)
      references DIA (ID_DIA)
      on delete restrict on update restrict;

alter table CAMBIO
   add constraint FK_CAMBIO_RELATIONS_USUARIO foreign key (CODSIS)
      references USUARIO (CODSIS)
      on delete restrict on update restrict;

alter table CARRERA
   add constraint FK_CARRERA_RELATIONS_DEPARTAM foreign key (ID_DEPARTAMENTO)
      references DEPARTAMENTO (ID_DEPARTAMENTO)
      on delete restrict on update restrict;

alter table DEPARTAMENTO
   add constraint FK_DEPARTAM_RELATIONS_USUARIO foreign key (CODSIS)
      references USUARIO (CODSIS)
      on delete restrict on update restrict;

alter table GRUPO
   add constraint FK_GRUPO_RELATIONS_GESTION foreign key (ID_GESTION)
      references GESTION (ID_GESTION)
      on delete restrict on update restrict;

alter table GRUPO
   add constraint FK_GRUPO_RELATIONS_USUARIO foreign key (CODSIS)
      references USUARIO (CODSIS)
      on delete restrict on update restrict;

alter table GRUPO
   add constraint FK_GRUPO_RELATIONS_MATERIA foreign key (COD_MATERIA)
      references MATERIA (COD_MATERIA)
      on delete restrict on update restrict;

alter table HISTORIAL
   add constraint FK_HISTORIA_RELATIONS_USUARIO foreign key (CODSIS)
      references USUARIO (CODSIS)
      on delete restrict on update restrict;

alter table HORARIO
   add constraint FK_HORARIO_RELATIONS_PERIODO foreign key (ID_PERIODO)
      references PERIODO (ID_PERIODO)
      on delete restrict on update restrict;

alter table HORARIO
   add constraint FK_HORARIO_RELATIONS_AMBIENTE foreign key (NOMBRE_AMBIENTE)
      references AMBIENTE (NOMBRE_AMBIENTE)
      on delete restrict on update restrict;

alter table HORARIO
   add constraint FK_HORARIO_RELATIONS_GRUPO foreign key (ID_GRUPO)
      references GRUPO (ID_GRUPO)
      on delete restrict on update restrict;

alter table HORARIO
   add constraint FK_HORARIO_RELATIONS_DIA foreign key (ID_DIA)
      references DIA (ID_DIA)
      on delete restrict on update restrict;

alter table MATERIA
   add constraint FK_MATERIA_RELATIONS_CARRERA foreign key (COD_CARRERA)
      references CARRERA (COD_CARRERA)
      on delete restrict on update restrict;

alter table NOTIFICACION
   add constraint FK_NOTIFICA_RELATIONS_USUARIO foreign key (CODSIS)
      references USUARIO (CODSIS)
      on delete restrict on update restrict;

alter table RECURSO_AMBIENTE
   add constraint FK_RECURSO__RELATIONS_AMBIENTE foreign key (NOMBRE_AMBIENTE)
      references AMBIENTE (NOMBRE_AMBIENTE)
      on delete restrict on update restrict;

alter table RECURSO_AMBIENTE
   add constraint FK_RECURSO__RELATIONS_RECURSO foreign key (ID_RECURSO)
      references RECURSO (ID_RECURSO)
      on delete restrict on update restrict;

alter table SOLICITUD_CAMBIO
   add constraint FK_SOLICITU_RELATIONS_CAMBIO foreign key (ID_CAMBIO)
      references CAMBIO (ID_CAMBIO)
      on delete restrict on update restrict;

alter table SOLICITUD_CAMBIO
   add constraint FK_SOLICITU_RELATIONS_TIPO_SOL foreign key (ID_TIPO_SOLICITUD)
      references TIPO_SOLICITUD (ID_TIPO_SOLICITUD)
      on delete restrict on update restrict;

alter table SOLICITUD_CAMBIO
   add constraint FK_SOLICITU_RELATIONS_ESTADO_S foreign key (EST_ID_SOLICITUD)
      references ESTADO_SOLICITUD (ID_SOLICITUD)
      on delete restrict on update restrict;

alter table SOLICITUD_CAMBIO
   add constraint FK_SOLICITU_RELATIONS_GESTION foreign key (ID_GESTION)
      references GESTION (ID_GESTION)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_RELATIONS_CARGO foreign key (ID_CARGO)
      references CARGO (ID_CARGO)
      on delete restrict on update restrict;

